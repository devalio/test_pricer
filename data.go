package main

import (
	"encoding/csv"
	"errors"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type (
	// Functions of this type are used to save records to the database
	updater func(Price) (updateResult, error)
	// Functions of this type are used to retrieve data from a database with specified parameters
	getter func([]*Sorting, int64, int64) (listPricesResult, error)
	// Price is basic structural unit
	Price struct {
		ProductName  string    `bson:"product_name"`
		Price        float64   `bson:"price"`
		ChangesCount int32     `bson:"changes_count"`
		LastChange   time.Time `bson:"last_change"`
	}
	// The result of a function that stores data
	updateResult struct {
		Added   int64
		Updated int64
	}
	// The result of a function that list prices
	listPricesResult []Price
)

// toResultList converts the data of the structure listPricesResult to the structure ResultList
func (r listPricesResult) toResultList() *ResultList {
	if len(r) == 0 {
		return nil
	}
	var data = make([]*ResultList_ResultElement, 0, len(r))
	for _, row := range r {
		data = append(data, &ResultList_ResultElement{
			ProductName:  row.ProductName,
			Price:        row.Price,
			ChangesCount: row.ChangesCount,
			LastChange:   row.LastChange.Unix(),
		})
	}
	return &ResultList{Data: data}
}

const (
	// default limit parameter value
	defaultLimit = 20
	// maximum for limit parameter value
	maxLimit = 100
)

// listPricesBy selects data with the specified sorting and paging parameters and converts
// the received data into the ResultList structure.
//  Uses constants defaultLimit and maxLimit to check paging parameters.
func listPricesBy(getter getter, sorting []*Sorting, limit, skip int64) (result *ResultList, err error) {
	if limit > maxLimit {
		err = errors.New("exceeded the maximum allowed limit")
		return
	}
	if limit < 1 {
		limit = defaultLimit
	}
	var data listPricesResult
	if data, err = getter(sorting, limit, skip); err == nil {
		result = data.toResultList()
	} else {
		log.Printf("<%T>: %s\n", err, err)
	}
	return
}

// recordToPrice converts an array of string values from a text file into Price data
func recordToPrice(record []string) (price Price, err error) {
	if len(record) != 2 {
		err = errors.New("wrong column count")
		log.Printf("<%T>: %s\n", err, err)
		return
	}
	price.Price, err = strconv.ParseFloat(strings.TrimSpace(record[1]), 64)
	if err == nil {
		price.ProductName = record[0]
	} else {
		log.Printf("<%T>: %s\n", err, err)
	}
	return
}

// parseRemoteList reads data from the file line by line, transfers the converted values of Price structure to fetch data channel.
// Recognizes data separated by a character ";". If an error is detected, the process is stopped.
//  Closes the channel after completion.
func parseRemoteList(data io.Reader, fetch chan<- Price) (err error) {
	var (
		lastChange = time.Now()
		csvReader  = csv.NewReader(data)
	)
	csvReader.Comma = ';'
	defer close(fetch)
	for {
		var (
			record []string
			price  Price
		)
		if record, err = csvReader.Read(); err != nil && err != io.EOF {
			log.Printf("<%T>: %s\n", err, err)
		}
		if err != nil || record == nil {
			break
		}
		if price, err = recordToPrice(record); err != nil {
			break
		}
		price.LastChange = lastChange
		fetch <- price
	}
	if err == io.EOF {
		return nil
	}
	return err
}

// saveParsedPrices saves the data that is read from fetch channel.
// After saving all data (when the channel is closed), executes onDone.
func saveParsedPrices(updater updater, fetch <-chan Price, onDone func()) {
	var (
		linesCnt    int64
		upsertedCnt int64
		modifiedCnt int64
	)
	defer onDone()
	for price := range fetch {
		linesCnt++
		result, err := updater(price)
		if err != nil {
			log.Printf("skipped record: %s\n", price.ProductName)
		} else {
			upsertedCnt += result.Updated
			modifiedCnt += result.Added
		}
	}
	log.Println(struct {
		LinesCount int64
		Upserted   int64
		Modified   int64
	}{
		linesCnt,
		upsertedCnt,
		modifiedCnt,
	})
}

const (
	// time in seconds to execute processRemotePriceList
	remotePriceListWorkerTimeout = 30
)

// processRemotePriceList captures the remote price list according to the specified URL,
// parses the received data and saves using the specified saving procedure.
//  uses the remotePriceListWorkerTimeout constant to limit the save execution time
func processRemotePriceList(updater updater, url string) error {
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("<%T>: %s\n", err, err)
		return err
	}
	defer func() {
		if e := resp.Body.Close(); e != nil {
			log.Printf("<%T> on body.Close: %s\n", err, err)
		}
	}()
	var (
		parsed = make(chan Price)
		closer = make(chan interface{})
	)
	go saveParsedPrices(updater, parsed, func() {
		close(closer)
	})
	if err = parseRemoteList(resp.Body, parsed); err != nil {
		log.Printf("<%T>: %s\n", err, err)
	}
	select {
	case <-closer:
		break
	case <-time.After(remotePriceListWorkerTimeout * time.Second):
		log.Println("Price list saving timeout exceeded")
	}
	return err
}
