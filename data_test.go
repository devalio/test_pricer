package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net"
	"net/http"
	"reflect"
	"strconv"
	"sync"
	"testing"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func Test_recordToPrice(t *testing.T) {
	type args struct {
		record []string
	}
	tests := []struct {
		name      string
		args      args
		wantPrice Price
		wantErr   bool
	}{
		{
			name: "simple test (int)",
			args: args{record: []string{"some product name", "123"}},
			wantPrice: Price{
				ProductName: "some product name",
				Price:       123,
			},
			wantErr: false,
		},
		{
			name: "with spaces (int)",
			args: args{record: []string{" some product name ", " 123 "}},
			wantPrice: Price{
				ProductName: " some product name ",
				Price:       123,
			},
			wantErr: false,
		},
		{
			name: "simple test (float with dot)",
			args: args{record: []string{"some product name", " 123.66"}},
			wantPrice: Price{
				ProductName: "some product name",
				Price:       123.66,
			},
			wantErr: false,
		},
		{
			name:      "simple wrong (float with comma)",
			args:      args{record: []string{"some product name", " 123,66"}},
			wantPrice: Price{},
			wantErr:   true,
		},
		{
			name:      "simple wrong (wrong format)",
			args:      args{record: []string{"some product name", "abc"}},
			wantPrice: Price{},
			wantErr:   true,
		},
		{
			name:      "wrong columns count 1",
			args:      args{record: []string{"some product name", "1", "2"}},
			wantPrice: Price{},
			wantErr:   true,
		},
		{
			name:      "wrong columns count 2",
			args:      args{record: []string{"some product name"}},
			wantPrice: Price{},
			wantErr:   true,
		},
		{
			name:      "nil argument",
			args:      args{record: nil},
			wantPrice: Price{},
			wantErr:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPrice, err := recordToPrice(tt.args.record)
			if (err != nil) != tt.wantErr {
				t.Errorf("recordToPrice() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotPrice, tt.wantPrice) {
				t.Errorf("recordToPrice() gotPrice = %v, want %v", gotPrice, tt.wantPrice)
			}
		})
	}
}

func Test_saveParsedPrices(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	simpleTestCaseData := func() []Price {
		return []Price{
			{
				ProductName: "some product",
				Price:       123,
			},
			{
				ProductName: "other product",
				Price:       321,
			},
			{
				ProductName: "third product",
				Price:       1999.99,
			},
		}
	}
	makePriceChannel := func(data []Price, async bool) <-chan Price {
		ch := make(chan Price)
		go func() {
			defer close(ch)
			if !async {
				for _, price := range data {
					ch <- price
				}
			} else {
				var (
					ch2 = make([]chan Price, 10)
					g   sync.WaitGroup
				)
				g.Add(10)
				for n := 0; n < 10; n++ {
					ch2[n] = make(chan Price)
					go func(myChannel <-chan Price) {
						defer g.Done()
						for p := range myChannel {
							ch <- p
						}
					}(ch2[n])
				}
				for _, price := range data {
					ch2[rand.Intn(10)] <- price
				}
				for n := 0; n < 10; n++ {
					close(ch2[n])
				}
				// first make sure all workers are done, because exiting the function will close the channel
				g.Wait()
			}
		}()
		return ch
	}
	type (
		args struct {
			fetch <-chan Price
		}
		testType struct {
			name string
			args args
			test func([]Price) error
		}
	)
	makeLondDataTest := func(name string, cnt int) testType {
		data := make([]Price, 0, cnt)
		for n := 0; n < cnt; n++ {
			data = append(data, Price{
				ProductName:  fmt.Sprintf("product N: %d", rand.Int()),
				Price:        rand.Float64(),
				ChangesCount: rand.Int31n(100),
				LastChange:   time.Now().Add(time.Minute * time.Duration(rand.Intn(100))),
			})
		}
		return testType{
			name: name,
			args: args{fetch: makePriceChannel(data, true)},
			test: func(result []Price) error {
				var (
					checkList = make([]*bool, len(data), len(data))
					testIsOK  = true
				)
				for _, r := range result {
					var matched = false
					for n, x := range data {
						if reflect.DeepEqual(r, x) {
							if checkList[n] != nil {
								// it can happen by accident, but the chances are extremely small
								return errors.New("duplicate entry found")
							}
							checkList[n] = &testIsOK
							matched = true
							break
						}
					}
					if !matched {
						return errors.New("not satisfying record found")
					}
				}
				var notChecked = 0
				for _, f := range checkList {
					if f == nil {
						notChecked++
					}
				}
				if notChecked > 0 {
					return errors.New(strconv.Itoa(notChecked) + " missed records found")
				}
				return nil
			},
		}
	}
	tests := []testType{
		{
			name: "simple test",
			args: args{
				fetch: makePriceChannel(simpleTestCaseData(), false),
			},
			test: func(data []Price) error {
				if !reflect.DeepEqual(data, simpleTestCaseData()) {
					return errors.New("data match error")
				}
				return nil
			},
		},
		makeLondDataTest("async (hundred rows)", 1000),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var data []Price
			updater := func(p Price) (updateResult, error) {
				data = append(data, p)
				return updateResult{}, nil
			}
			closer := make(chan interface{})
			saveParsedPrices(updater, tt.args.fetch, func() {
				close(closer)
			})
			select {
			case <-closer:
				break
			case <-time.After(time.Second):
				t.Error("task completed signal not received")
			}
			if err := tt.test(data); err != nil {
				t.Error(err)
			}
		})
	}
}

type (
	remotePricelistServer struct {
		path      string
		testParam string
		data      string
	}
)

func (s remotePricelistServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == s.path && r.URL.Query().Get("testParam") == s.testParam {
		_, err := w.Write([]byte(s.data))
		if err != nil {
			panic(err)
		}
	}
}

func Test_getRemotePriceList(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		args    args
		start   func() interface{}
		final   func(interface{})
		test    func(string) error
		wantErr bool
	}{
		{
			name: "smoke",
			args: args{
				url: "http://127.0.0.1:8181/test_path/test_script.php?testParam=test#some_hash",
			},
			start: func() interface{} {
				if listener, err := net.Listen("tcp", "127.0.0.1:8181"); err != nil {
					panic("cannot start http server")
				} else {
					go http.Serve(listener, remotePricelistServer{
						path:      "/test_path/test_script.php",
						testParam: "test",
						data:      "",
					})
					var trying = 100 // 2 sec
					for {
						_, err := http.Get("http://127.0.0.1:8181/test")
						if err == nil {
							break
						}
						trying--
						if trying < 0 {
							panic("cannot start listen")
						} else {
							time.Sleep(time.Millisecond * 20)
						}
					}
					return listener
				}
			},
			final: func(i interface{}) {
				i.(net.Listener).Close()
			},
			test: func(string) error {
				return nil
			},
			wantErr: false,
		},
		{
			name: "check data",
			args: args{
				url: "http://127.0.0.1:8182/test_path/test_script.php?testParam=test#some_hash",
			},
			start: func() interface{} {
				if listener, err := net.Listen("tcp", "127.0.0.1:8182"); err != nil {
					panic("cannot start http server")
				} else {
					go http.Serve(listener, remotePricelistServer{
						path:      "/test_path/test_script.php",
						testParam: "test",
						data:      "some product name;99.1\nsome other;15.5",
					})
					var trying = 100 // 2 sec
					for {
						_, err := http.Get("http://127.0.0.1:8182/test")
						if err == nil {
							break
						}
						trying--
						if trying < 0 {
							panic("cannot start listen")
						} else {
							time.Sleep(time.Millisecond * 20)
						}
					}
					return listener
				}
			},
			final: func(i interface{}) {
				i.(net.Listener).Close()
			},
			test: func(s string) error {
				if s != "\"some product name\";99.10\n\"some other\";15.50\n" {
					return fmt.Errorf("got: '%s'", s)
				}
				return nil
			},
			wantErr: false,
		},
		{
			name: "data error",
			args: args{
				url: "http://127.0.0.1:8183/test_path/test_script.php?testParam=test#some_hash",
			},
			start: func() interface{} {
				if listener, err := net.Listen("tcp", "127.0.0.1:8183"); err != nil {
					panic("cannot start http server")
				} else {
					go http.Serve(listener, remotePricelistServer{
						path:      "/test_path/test_script.php",
						testParam: "test",
						data:      "<HTML><body><h2>Something went wrong; Error 404!</h2></body></HTML>",
					})
					var trying = 100 // 2 sec
					for {
						_, err := http.Get("http://127.0.0.1:8183/test")
						if err == nil {
							break
						}
						trying--
						if trying < 0 {
							panic("cannot start listen")
						} else {
							time.Sleep(time.Millisecond * 20)
						}
					}
					return listener
				}
			},
			final: func(i interface{}) {
				i.(net.Listener).Close()
			},
			test: func(s string) error {
				return nil
			},
			wantErr: true,
		},
		{
			name: "err remote server",
			args: args{
				url: "http://127.0.0.1:8193/test_path/test_script.php?testParam=test#some_hash",
			},
			start:   func() interface{} { return nil },
			final:   func(i interface{}) {},
			test:    func(s string) error { return nil },
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var testDataBuff = bytes.NewBufferString("")
			i := tt.start()
			defer tt.final(i)
			updater := func(p Price) (u updateResult, e error) {
				_, e = testDataBuff.Write([]byte(fmt.Sprintf(
					"\"%s\";%0.2f\n",
					p.ProductName,
					p.Price,
				)))
				return
			}
			if err := processRemotePriceList(updater, tt.args.url); (err != nil) != tt.wantErr {
				t.Errorf("processRemotePriceList() error = %v, wantErr %v", err, tt.wantErr)
			} else if err := tt.test(testDataBuff.String()); err != nil {
				t.Error(err)
			}
		})
	}
}

func Test_parseRemoteList(t *testing.T) {
	type args struct {
		data io.Reader
	}
	tests := []struct {
		name    string
		args    args
		test    func(<-chan Price) error
		wantErr bool
	}{
		{
			name: "smoke",
			args: args{data: bytes.NewBufferString("product1;0.00")},
			test: func(prices <-chan Price) error {
				for range prices {
				}
				return nil
			},
			wantErr: false,
		},
		{
			name: "check data",
			args: args{data: bytes.NewBufferString("product1;1.00\nproduct2;2.00\n")},
			test: func(prices <-chan Price) error {
				var checkInt = 0
				for p := range prices {
					if p.ProductName == "product1" && p.Price == 1 {
						checkInt += 1
					} else if p.ProductName == "product2" && p.Price == 2 {
						checkInt += 2
					} else {
						return errors.New("wrong data")
					}
				}
				if checkInt != 3 {
					return errors.New("wrong data")
				}
				return nil
			},
			wantErr: false,
		},
		{
			name: "wrong column count 1",
			args: args{data: bytes.NewBufferString("product1 some without price")},
			test: func(prices <-chan Price) error {
				for range prices {
				}
				return nil
			},
			wantErr: true,
		},
		{
			name: "wrong column count 2",
			args: args{data: bytes.NewBufferString("product;123;321")},
			test: func(prices <-chan Price) error {
				for range prices {
				}
				return nil
			},
			wantErr: true,
		},
		{
			name: "syntax error",
			args: args{data: bytes.NewBufferString("product1;abc.00")},
			test: func(prices <-chan Price) error {
				for range prices {
				}
				return nil
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var (
				fetch  = make(chan Price)
				closer = make(chan Price)
			)
			go func() {
				if err := tt.test(fetch); err != nil {
					t.Error(err)
				}
				close(closer)
			}()
			if err := parseRemoteList(tt.args.data, fetch); (err != nil) != tt.wantErr {
				t.Errorf("parseRemoteList() error = %v, wantErr %v", err, tt.wantErr)
			}
			<-closer
		})
	}
}

func Test_listPricesBy(t *testing.T) {
	type args struct {
		getter  getter
		sorting []*Sorting
		limit   int64
		skip    int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "default argms",
			args: args{
				getter: func(sortings []*Sorting, l int64, s int64) (listPricesResult, error) {
					if sortings != nil || l != defaultLimit || s != 0 {
						return nil, errors.New("wrong default parameter values")
					}
					return nil, nil
				},
				sorting: nil,
				limit:   0,
				skip:    0,
			},
			wantErr: false,
		},
		{
			name: "limit parameter bounds",
			args: args{
				getter: func(sortings []*Sorting, l int64, s int64) (listPricesResult, error) {
					if sortings != nil || l != defaultLimit || s != 0 {
						return nil, errors.New("wrong default parameter values")
					}
					return nil, nil
				},
				sorting: nil,
				limit:   maxLimit + 1,
				skip:    0,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := listPricesBy(tt.args.getter, tt.args.sorting, tt.args.limit, tt.args.skip)
			if (err != nil) != tt.wantErr {
				t.Errorf("listPricesBy() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
