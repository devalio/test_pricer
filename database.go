package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/url"
	"os"
	"time"
)

const (
	mongoSocketTimeout          = 15
	mongoConnectionTimeout      = 15
	mongoServerSelectionTimeout = 15

	pricesCollection = "prices"
)

// connectToDB returns a handle to the client database connection.
// Parameters are set by environment variables DB_USER_NAME, DB_PASSWORD, DB_HOST, DB_PORT (27017 by default).
func connectToDB() (client *mongo.Client, err error) {
	var (
		dbUserName = os.Getenv("DB_USER_NAME")
		dbPassword = os.Getenv("DB_PASSWORD")
		dbHost     = os.Getenv("DB_HOST")
		dbPort     = os.Getenv("DB_PORT")
	)
	if dbPort == "" {
		dbPort = "27017"
	}
	log.Printf("init db connection: %s@%s:%s", dbUserName, dbHost, dbPort)
	if client, err = createMongoDbClient(dbHost, dbPort, dbUserName, dbPassword); err != nil {
		return
	}
	if err = client.Connect(context.Background()); err != nil {
		log.Printf("<%T>: %s\n", err, err)
		return
	}
	err = testMongoDbClient(client)
	return
}

// makeMongoDbConnectionURI creates a connection string according to the specified parameters.
func makeMongoDbConnectionURI(host, port, user, password string) string {
	return fmt.Sprintf(
		"mongodb://%s:%s@%s:%s",
		url.QueryEscape(user),
		url.QueryEscape(password),
		host,
		port,
	)
}

// createMongoDbClient creates a new client to connect to the database with the given parameters.
// Uses mongoSocketTimeout, mongoConnectionTimeout and mongoServerSelectionTimeout to set timeouts
func createMongoDbClient(host, port, user, password string) (client *mongo.Client, err error) {
	client, err = mongo.NewClient(
		options.Client().ApplyURI(makeMongoDbConnectionURI(host, port, user, password)).
			SetSocketTimeout(time.Second * mongoSocketTimeout).
			SetConnectTimeout(time.Second * mongoConnectionTimeout).
			SetServerSelectionTimeout(time.Second * mongoServerSelectionTimeout),
	)
	if err != nil {
		log.Printf("<%T>: %s\n", err, err)
	}
	return
}

// Ping the database
func testMongoDbClient(client *mongo.Client) error {
	err := client.Ping(context.Background(), nil)
	if err != nil {
		log.Printf("<%T>: %s\n", err, err)
	}
	return err
}

// Attempts to insert a new record into the database, if a record exists, no change occurs.
// If successful, the first argument of the result will indicate the number of inserted records and will be equal to one.
func tryToInsertSkipIfExists(ctx context.Context, c *mongo.Collection, price Price) (int64, error) {
	result, err := c.UpdateOne(
		ctx,
		bson.D{
			{"product_name", price.ProductName},
		},
		bson.D{
			{"$set", bson.D{{"product_name", price.ProductName}}},
			{"$setOnInsert", bson.D{{"price", price.Price}}},
			{"$setOnInsert", bson.D{{"changes_count", 1}}},
			{"$setOnInsert", bson.D{{"last_change", price.LastChange}}},
		},
		options.Update().SetUpsert(true),
	)
	if err != nil {
		log.Printf("<%T>: %s\n", err, err)
		return 0, err
	}
	return result.UpsertedCount, nil
}

// Attempts to update a record in the database.
// If successful, the first argument of the result will indicate the number of changed records and will be non-zero.
// The change counter (last_change field) is automatically incremented.
func tryToModifyPrice(ctx context.Context, c *mongo.Collection, price Price) (int64, error) {
	result, err := c.UpdateOne(
		ctx,
		bson.M{
			"product_name": bson.M{"$eq": price.ProductName},
			"price":        bson.M{"$ne": price.Price},
		},
		bson.D{
			{"$inc", bson.D{{"changes_count", 1}}},
			{"$set", bson.D{{"price", price.Price}}},
			{"$set", bson.D{{"last_change", price.LastChange}}},
		},
		options.Update().SetUpsert(false),
	)
	if err != nil {
		log.Printf("<%T>: %s\n", err, err)
		return 0, err
	}
	return result.ModifiedCount, nil
}

// makeDatabaseUpdater creates a procedure that implements saving the item to the database.
func makeDatabaseUpdater(ctx context.Context, db *mongo.Database) updater {
	collection := db.Collection(pricesCollection)
	return func(price Price) (result updateResult, err error) {
		if result.Updated, err = tryToModifyPrice(ctx, collection, price); err != nil {
			log.Printf("<%T>: %s\n", err, err)
			return
		}
		if result.Updated == 0 {
			if result.Added, err = tryToInsertSkipIfExists(ctx, collection, price); err != nil {
				log.Printf("<%T>: %s\n", err, err)
			}
		}
		return
	}
}

// makePricesGetter creates a procedure for selecting from a database with the specified sorting and paging parameters.
func makePricesGetter(ctx context.Context, db *mongo.Database) getter {
	collection := db.Collection(pricesCollection)
	return func(sorting []*Sorting, limit, skip int64) (data listPricesResult, err error) {
		var (
			findOption = options.Find()
			cursor     *mongo.Cursor
		)
		descToAscBsonFmt := func(desc bool) int {
			if desc {
				return -1
			}
			return 1
		}
		if len(sorting) > 0 {
			sortingD := bson.D{}
			for _, sort := range sorting {
				sortingD = append(sortingD, bson.E{string(sort.Field.String()), descToAscBsonFmt(sort.Desc)})
			}
			findOption = findOption.SetSort(sortingD)
		}
		findOption = findOption.SetSkip(skip).SetLimit(limit)
		if cursor, err = collection.Find(ctx, bson.D{}, findOption); err != nil {
			log.Printf("<%T>: %s\n", err, err)
			return
		}
		defer func() {
			if e := cursor.Close(ctx); e != nil {
				log.Printf("<%T> on cursor.Close: %s", e, e)
			}
		}()
		data = make([]Price, 0, limit)
		if err = cursor.All(ctx, &data); err != nil {
			log.Printf("<%T>: %s\n", err, err)
		}
		return
	}
}
