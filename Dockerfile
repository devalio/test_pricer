FROM ubuntu
RUN mkdir -p /usr/local/pricer
WORKDIR /usr/local/pricer
COPY ./bin/pricer /usr/local/pricer/pricer
EXPOSE 8080
STOPSIGNAL SIGINT
ENTRYPOINT ["/usr/local/pricer/pricer"]
