package main

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"math"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"sync"
	"testing"
	"time"
)

const testDatabaseName = "test"

type connectionArgms struct {
	host     string
	port     string
	user     string
	password string
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var dbArgms = connectionArgms{
	host:     os.Getenv("TEST_DB_HOST"),
	port:     os.Getenv("TEST_DB_PORT"),
	user:     os.Getenv("TEST_DB_USER"),
	password: os.Getenv("TEST_DB_PASS"),
}

func Test_createMongoDbClient(t *testing.T) {
	tests := []struct {
		name    string
		args    connectionArgms
		test    func(*mongo.Client) error
		wantErr bool
	}{
		{
			name: "connection ping",
			args: dbArgms,
			test: func(c *mongo.Client) error {
				defer c.Disconnect(context.Background())
				if err := c.Connect(context.Background()); err != nil {
					return err
				}
				return c.Ping(context.Background(), nil)
			},
			wantErr: false,
		},
		{
			name: "wrong password",
			args: connectionArgms{
				host:     dbArgms.host,
				port:     dbArgms.port,
				user:     dbArgms.user,
				password: dbArgms.password + "-some",
			},
			test: func(c *mongo.Client) error {
				// not connected
				if err := c.Connect(context.Background()); err != nil {
					return err
				}
				return c.Ping(context.Background(), nil)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotClient, err := createMongoDbClient(tt.args.host, tt.args.port, tt.args.user, tt.args.password)
			if err = tt.test(gotClient); err != nil && !tt.wantErr {
				t.Errorf("test error: %s", err)
			} else if err == nil && tt.wantErr {
				t.Error("want error")
			}
		})
	}
}

func makeClient(t *testing.T, close bool) *mongo.Client {
	client, err := createMongoDbClient(dbArgms.host, dbArgms.port, dbArgms.user, dbArgms.password)
	if err != nil {
		t.Errorf("connection error: %s", err)
	}
	err = client.Connect(context.Background())
	if err != nil {
		t.Errorf("connection error: %s", err)
	}
	if close {
		client.Disconnect(context.Background())
	}
	return client
}

func Test_testMongoDbClient(t *testing.T) {
	type args struct {
		client *mongo.Client
	}
	tests := []struct {
		name    string
		args    args
		final   func(*mongo.Client)
		wantErr bool
	}{
		{
			name: "connected and tested",
			args: args{client: makeClient(t, false)},
			final: func(client *mongo.Client) {
				if err := client.Disconnect(context.Background()); err != nil {
					t.Error(err)
				}
			},
			wantErr: false,
		},
		{
			name:    "closed connection",
			args:    args{client: makeClient(t, true)},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if tt.final != nil {
					tt.final(tt.args.client)
				}
			}()
			if err := testMongoDbClient(tt.args.client); (err != nil) != tt.wantErr {
				t.Errorf("testMongoDbClient() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_tryToInsertSkipIfExists(t *testing.T) {
	type args struct {
		ctx   context.Context
		c     *mongo.Client
		price Price
	}
	client := makeClient(t, false)
	if client == nil {
		t.Error("cannot connect")
		return
	}
	collection := client.Database(testDatabaseName).Collection(t.Name())
	_, err := collection.DeleteMany(
		context.Background(),
		bson.M{"product_name": bson.M{"$ne": "permanent"}},
	)
	if err != nil {
		t.Error(err)
	}
	if _, err = tryToInsertSkipIfExists(context.Background(), collection, Price{
		ProductName:  "test-product-1",
		Price:        100,
		ChangesCount: 1,
		LastChange:   time.Now(),
	}); err != nil {
		t.Error(err)
	}
	if err = client.Disconnect(context.Background()); err != nil {
		t.Error(err)
	}

	tests := []struct {
		name    string
		args    args
		test    func(*mongo.Collection, Price, int64) error
		final   func(client *mongo.Client)
		wantErr bool
	}{
		{
			name: "insertion test",
			args: args{
				ctx: context.Background(),
				c:   makeClient(t, false),
				price: Price{
					ProductName:  "some-product-" + strconv.Itoa(rand.Int()),
					Price:        rand.Float64(),
					ChangesCount: rand.Int31(),
					LastChange:   time.Now(),
				},
			},
			test: func(c *mongo.Collection, p Price, i int64) error {
				if i != 1 {
					return errors.New("returns must be = 1")
				}
				cur, err := c.Find(context.Background(),
					bson.D{
						{"product_name", p.ProductName},
						{"price", p.Price},
						{"last_change", p.LastChange},
						{"changes_count", 1}, // for insertion, this value is always "1"
					},
				)
				if err != nil {
					return err
				}
				if cur.Next(context.Background()) {
					return cur.Err()
				}
				return errors.New("cannot find inserted record")
			},
			final: func(client *mongo.Client) {
				if err := client.Disconnect(context.Background()); err != nil {
					t.Error(err)
				}
			},
			wantErr: false,
		},
		{
			name: "exists",
			args: args{
				ctx: context.Background(),
				c:   makeClient(t, false),
				price: Price{
					ProductName: "test-product-1",
					Price:       100,
					LastChange:  time.Now(),
				},
			},
			test: func(c *mongo.Collection, p Price, i int64) error {
				if i != 0 {
					return errors.New("returns must be = 0")
				}
				cur, err := c.Find(context.Background(),
					bson.D{
						{"product_name", p.ProductName},
						{"price", p.Price},
					},
				)
				if err != nil {
					return err
				}
				if cur.Next(context.Background()) {
					return cur.Err()
				}
				return errors.New("cannot find inserted record")
			},
			final: func(client *mongo.Client) {
				if err := client.Disconnect(context.Background()); err != nil {
					t.Error(err)
				}
			},
			wantErr: false,
		},
		{
			name: "broken connection",
			args: args{
				ctx: context.Background(),
				c:   makeClient(t, true),
				price: Price{
					ProductName: "test-product-2",
					LastChange:  time.Now(),
				},
			},
			test: func(c *mongo.Collection, p Price, i int64) error {
				return nil
			},
			wantErr: true,
		},
	}
	collectionName := t.Name()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if tt.final != nil {
					tt.final(tt.args.c)
				}
			}()
			got, err := tryToInsertSkipIfExists(
				tt.args.ctx,
				tt.args.c.Database(testDatabaseName).Collection(collectionName),
				tt.args.price,
			)
			if (err != nil) != tt.wantErr {
				t.Errorf("tryToInsertSkipIfExists() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil {
				if err = tt.test(tt.args.c.Database(testDatabaseName).Collection(collectionName), tt.args.price, got); err != nil {
					t.Errorf("error comparing: %s", err)
				}
			}
		})
	}
}

func Test_tryToModifyPrice(t *testing.T) {
	type args struct {
		ctx   context.Context
		c     *mongo.Client
		price Price
	}

	client := makeClient(t, false)
	if client == nil {
		t.Error("cannot connect")
		return
	}
	collection := client.Database(testDatabaseName).Collection(t.Name())
	_, err := collection.DeleteMany(
		context.Background(),
		bson.M{"product_name": bson.M{"$ne": "permanent"}},
	)
	if err != nil {
		t.Error(err)
	}
	if _, err = tryToInsertSkipIfExists(context.Background(), collection, Price{
		ProductName:  "test-product-1",
		Price:        100,
		ChangesCount: 1,
		LastChange:   time.Now(),
	}); err != nil {
		t.Error(err)
	}
	tests := []struct {
		name    string
		args    args
		test    func(*mongo.Collection, Price, int64) error
		final   func(client *mongo.Client)
		wantErr bool
	}{
		{
			name: "test changes_count increment",
			args: args{
				ctx: context.Background(),
				c:   makeClient(t, false),
				price: Price{
					ProductName:  "test-product-1",
					Price:        123,
					ChangesCount: 2, // important
					LastChange:   time.Now(),
				},
			},
			test: func(c *mongo.Collection, p Price, i int64) error {
				if i != 1 {
					return errors.New("returns must be = 1")
				}
				cur, err := c.Find(context.Background(),
					bson.D{
						{"product_name", p.ProductName},
						{"price", p.Price},
						{"changes_count", p.ChangesCount},
					},
				)
				if err != nil {
					return err
				}
				if cur.Next(context.Background()) {
					return cur.Err()
				}
				return errors.New("cannot find inserted record")
			},
			final: func(client *mongo.Client) {
				if err := client.Disconnect(context.Background()); err != nil {
					t.Error(err)
				}
			},
			wantErr: false,
		},
		{
			name: "not exists",
			args: args{
				ctx: context.Background(),
				c:   makeClient(t, false),
				price: Price{
					ProductName: "test-product-2-nex",
					Price:       123,
					LastChange:  time.Now(),
				},
			},
			test: func(c *mongo.Collection, p Price, i int64) error {
				if i != 0 {
					return errors.New("returns must be = 0")
				}
				cur, err := c.Find(context.Background(),
					bson.D{
						{"product_name", p.ProductName},
						{"price", p.Price},
					},
				)
				if err != nil {
					return err
				}
				if cur.Next(context.Background()) {
					return errors.New("matches not expected")
				}
				return nil
			},
			final: func(client *mongo.Client) {
				if err := client.Disconnect(context.Background()); err != nil {
					t.Error(err)
				}
			},
			wantErr: false,
		},
		{
			name: "broken connection",
			args: args{
				ctx: context.Background(),
				c:   makeClient(t, true),
				price: Price{
					ProductName: "test-product-2",
					LastChange:  time.Now(),
				},
			},
			test: func(c *mongo.Collection, p Price, i int64) error {
				return nil
			},
			wantErr: true,
		},
	}
	collectionName := t.Name()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if tt.final != nil {
					tt.final(tt.args.c)
				}
			}()
			got, err := tryToModifyPrice(tt.args.ctx, tt.args.c.Database(testDatabaseName).Collection(collectionName), tt.args.price)
			if (err != nil) != tt.wantErr {
				t.Errorf("tryToModifyPrice() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil {
				if err = tt.test(tt.args.c.Database(testDatabaseName).Collection(collectionName), tt.args.price, got); err != nil {
					t.Errorf("error comparing: %s", err)
				}
			}
		})
	}
}

func makeDataFn(prefix string, cnt int) []Price {
	var (
		prices     = make([]Price, 0, cnt)
		changeTime = time.Now()
	)
	for nn := 0; nn < cnt; nn++ {
		prices = append(prices, Price{
			ProductName: prefix + "-prod-" + strconv.Itoa(rand.Int()),
			Price:       math.Round(rand.Float64()*10000) / 100,
			LastChange:  changeTime,
		})
	}
	return prices
}

func Test_makeDatabaseUpdater(t *testing.T) {
	type args struct {
		ctx context.Context
		db  *mongo.Database
	}
	client := makeClient(t, false)
	if client == nil {
		t.Error("cannot connect")
		return
	}
	_, err := client.Database(testDatabaseName).Collection(pricesCollection).DeleteMany(
		context.Background(),
		bson.M{"product_name": bson.M{"$regex": `insert-\d+-prod-\d+`}},
	)
	if err != nil {
		t.Error(err)
	}
	defer client.Disconnect(context.Background())
	testFunc := func(p []Price) error {
		db := client.Database(testDatabaseName)
		for _, price := range p {
			r, err := db.Collection(pricesCollection).Find(
				context.Background(),
				bson.D{
					{"product_name", price.ProductName},
					{"price", price.Price},
				},
			)
			if err != nil {
				return err
			}
			if !r.Next(context.Background()) {
				return errors.New("record not found")
			}
		}
		return nil
	}
	type testStruct struct {
		name string
		args args
		data []Price
		test func([]Price) error
	}
	tests := []testStruct{
		{
			name: "insert-100",
			args: args{
				ctx: context.Background(),
				db:  client.Database(testDatabaseName),
			},
			data: makeDataFn("insert-100", 100),
			test: testFunc,
		},
		{
			name: "insert-1000",
			args: args{
				ctx: context.Background(),
				db:  client.Database(testDatabaseName),
			},
			data: makeDataFn("insert-1000", 1000),
			test: testFunc,
		},
		{
			name: "insert-500",
			args: args{
				ctx: context.Background(),
				db:  client.Database(testDatabaseName),
			},
			data: makeDataFn("insert-500", 500),
			test: testFunc,
		},
	}
	var wg sync.WaitGroup
	t.Parallel()
	for _, tt := range tests {
		wg.Add(1)
		go func(tt testStruct) {
			t.Run(tt.name, func(t *testing.T) {
				var updFn func(Price) (updateResult, error)
				if updFn = makeDatabaseUpdater(tt.args.ctx, tt.args.db); updFn == nil {
					t.Error("nil pointer unexpected")
				}
				for _, d := range tt.data {
					_, err := updFn(d)
					if err != nil {
						t.Error(err)
					}
				}
				if err := tt.test(tt.data); err != nil {
					t.Error(err)
				}
			})
			wg.Done()
		}(tt)
	}
	wg.Wait()
}

type sortedPrices struct {
	sorting []*Sorting
	prices  []*Price
}

func (p sortedPrices) Len() int {
	return len(p.prices)
}

func (p sortedPrices) Less(i, j int) bool {
	l, r := p.prices[i], p.prices[j]
	for _, sort := range p.sorting {
		switch sort.Field {
		case Sorting_product_name:
			if l.ProductName == r.ProductName {
				continue
			}
			return l.ProductName < r.ProductName
		case Sorting_price:
			if l.Price == r.Price {
				continue
			}
			return l.Price < r.Price
		case Sorting_changes_count:
			if l.ChangesCount == r.ChangesCount {
				continue
			}
			return l.ChangesCount < r.ChangesCount
		case Sorting_last_change:
			if l.LastChange.Equal(r.LastChange) {
				continue
			}
			return l.LastChange.Before(r.LastChange)
		}
	}
	return false
}

func (p sortedPrices) Swap(i, j int) {
	p.prices[i], p.prices[j] = p.prices[j], p.prices[i]
}

func comparePriceDataArray(l []Price, r []Price) bool {
	if len(l) != len(r) {
		return false
	}
	for i, p := range l {
		if p.ProductName != r[i].ProductName {
			return false
		}
		if p.Price != r[i].Price {
			return false
		}
		if !p.LastChange.Equal(r[i].LastChange) {
			return false
		}
	}
	return true
}

func Test_makePricesGetter(t *testing.T) {
	type args struct {
		ctx context.Context
		db  *mongo.Database
	}
	type args2 struct {
		sort  []*Sorting
		limit int64
		skip  int64
	}

	client := makeClient(t, false)
	if client == nil {
		t.Error("cannot connect")
		return
	}
	_, err := client.Database(testDatabaseName).Collection(pricesCollection).DeleteMany(
		context.Background(),
		bson.M{"product_name": bson.M{"$regex": `list-\d+-prod-\d+`}},
	)
	if err != nil {
		t.Error(err)
	}
	defer client.Disconnect(context.Background())

	updFn := makeDatabaseUpdater(context.Background(), client.Database(testDatabaseName))
	eData := makeDataFn("list-1000", 1000)
	for _, price := range eData {
		_, err := updFn(price)
		if err != nil {
			t.Error(err)
		}
	}

	getCorrectResult := func(a args2) listPricesResult {
		d := make([]*Price, len(eData))
		for i := range eData {
			d[i] = &eData[i]
		}
		p := sortedPrices{
			sorting: a.sort,
			prices:  d,
		}
		sort.Sort(p)
		r := make([]Price, 0, a.limit)
		last := int(a.limit + a.skip)
		if last > len(p.prices) {
			last = len(p.prices)
		}
		for _, e := range p.prices[a.skip:last] {
			r = append(r, *e)
		}
		return r
	}

	tests := []struct {
		name  string
		args  args
		args2 args2
	}{
		{
			name: "test (price,name)",
			args: args{
				ctx: context.Background(),
				db:  client.Database(testDatabaseName),
			},
			args2: args2{
				sort: []*Sorting{
					{
						Field: Sorting_price,
						Desc:  false,
					},
					{
						Field: Sorting_product_name,
						Desc:  false,
					},
				},
				limit: rand.Int63n(50) + 100,
				skip:  rand.Int63n(500),
			},
		},
		{
			name: "test (desc name)",
			args: args{
				ctx: context.Background(),
				db:  client.Database(testDatabaseName),
			},
			args2: args2{
				sort: []*Sorting{
					{
						Field: Sorting_product_name,
						Desc:  true,
					},
				},
				limit: rand.Int63n(50) + 100,
				skip:  rand.Int63n(500),
			},
		},
		{
			name: "tail",
			args: args{
				ctx: context.Background(),
				db:  client.Database(testDatabaseName),
			},
			args2: args2{
				sort: []*Sorting{
					{
						Field: Sorting_product_name,
						Desc:  false,
					},
				},
				limit: rand.Int63n(50) + 100,
				skip:  rand.Int63n(920),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var getter func(sorting []*Sorting, limit, skip int64) (data listPricesResult, err error)
			if getter = makePricesGetter(tt.args.ctx, tt.args.db); getter == nil {
				t.Error("unexpected nil pointer")
			}
			result, err := getter(tt.args2.sort, tt.args2.limit, tt.args2.skip)
			if err != nil {
				t.Error(err)
			}
			want := getCorrectResult(tt.args2)
			if comparePriceDataArray(result, want) {
				t.Error("matching error")
			}
		})
	}
}
