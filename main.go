package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
)

type (
	// Server is the gRPC service implementation.
	Server struct {
		mongoClient *mongo.Client
	}
)

// The name of the database given by the environment variable DB_NAME
var databaseName = os.Getenv("DB_NAME")

// getDatabase allows you to get a handle to the database with which the service is running.
func (s *Server) getDatabase() *mongo.Database {
	return s.mongoClient.Database(databaseName)
}

// Fetch starts the process of picking up a price list from a remote resource.
// Returns a simple boolean value that indicates success.
func (s *Server) Fetch(_ context.Context, r *RequestFetch) (*ResultFetch, error) {
	var (
		ctx = context.Background()
		db  = s.getDatabase()
	)
	if err := processRemotePriceList(makeDatabaseUpdater(ctx, db), r.Url); err != nil {
		log.Printf("processRemotePriceList <%T>: %s\n", err, err)
		return nil, err
	}
	return &ResultFetch{Result: true}, nil
}

// List allows you to get a selection from the price list with the specified sorting.
// The result is paginated.
func (s *Server) List(_ context.Context, r *RequestList) (*ResultList, error) {
	var (
		ctx = context.Background()
		db  = s.getDatabase()
	)
	return listPricesBy(makePricesGetter(ctx, db), r.Sortby, r.Limit, r.Skip)
}

func (s *Server) mustEmbedUnimplementedPricerServer() {
	// dummy
}

// Registers the gRPC service and starts listening on the network.
// Network parameters are set by environment variables: LISTEN_HOST, LISTEN_PORT (8080 by default).
// startHTTP is synchronous, exits when listener exits.
func startHTTP(server *Server) (err error) {
	var (
		host     = os.Getenv("LISTEN_HOST")
		port     = os.Getenv("LISTEN_PORT")
		addr     net.TCPAddr
		listener net.Listener
	)
	if port == "" {
		port = "8080"
	}
	if listener, err = net.Listen(addr.Network(), fmt.Sprintf("%s:%s", host, port)); err != nil {
		return
	}
	gRPC := grpc.NewServer()
	RegisterPricerServer(gRPC, server)
	// we launch the service in a separate routine
	go func() {
		err = gRPC.Serve(listener)
	}()
	// we remain to wait for the shutdown signal, then turn off the service correctly
	osSignalExit := make(chan os.Signal)
	signal.Notify(osSignalExit, syscall.SIGTERM, syscall.SIGINT)
	select {
	case <-osSignalExit:
		log.Println("Shutdown signal received")
		gRPC.GracefulStop()
	}
	return
}

func main() {
	mongoClient, err := connectToDB()
	if err != nil {
		log.Fatal(err)
	}
	var server = Server{
		mongoClient: mongoClient,
	}
	if err = startHTTP(&server); err != nil {
		log.Fatalf("<%T>: %s", err, err)
	}
}
