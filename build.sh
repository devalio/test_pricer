#!/bin/bash
SERVER="192.168.1.89"
APP_PATH="/usr/local/pricer"
mkdir -p ./bin &&
echo "build..." &&
GO111MODULE=on go build -o ./bin/pricer &&
echo "dockerize..." &&
docker build -t test/http . &&
docker tag test/http $SERVER:32000/test/http &&
echo "push..." &&
docker push $SERVER:32000/test/http &&
echo "put *.yml"|sftp $SERVER:$APP_PATH &&
echo "apply..." &&
ssh $SERVER "/snap/bin/microk8s.kubectl apply -f $APP_PATH/pricer-service.yml" &&
ssh $SERVER "/snap/bin/microk8s.kubectl apply -f $APP_PATH/mongo-test.yml" &&
echo "clean..." &&
rm -fR ./bin
